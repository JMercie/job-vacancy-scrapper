import puppeteer from 'puppeteer';

const browser = await puppeteer.launch({headless: false});
const page = await browser.newPage();
await page.goto('https://job-vacancy-champan-staging.herokuapp.com/register');
await page.$eval('#user_name', el => el.value = 'joseph');
await page.$eval('#user_email', el => {
  const random = Math.floor(Math.random() * 10).toString();
  el.value = `${random}-bot@thisisabot.com`
});
await page.$eval('#user_password', el => el.value = 'Passw0rd!1');
await page.$eval('#user_password_confirmation', el => el.value = 'Passw0rd!1');
await page.evaluate(() => {
  document.querySelector('body > div > div.container > form > input.btn.btn-primary').click();
})
await page.waitForNavigation();
